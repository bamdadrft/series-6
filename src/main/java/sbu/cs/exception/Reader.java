package sbu.cs.exception;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.List;

public class Reader {

    public void readTwitterCommands(List<String> args) throws ApException {
        for (String arg : args)
        {
            if (Util.getNotImplementedCommands().contains(arg))
            {
                throw new NotImplementedCommandException();
            }
        }

        for (String arg : args)
        {
            if (!Util.getImplementedCommands().contains(arg))
            {
                throw new UnrecognizedCommandException();
            }

        }
    }

    /**
     * function inputs are String but odd positions must be integer parsable
     * a valid input is like -> "ap", "2", "beheshti", "3992", "20"
     * throw BadInput exception when the string is not parsable.
     *
     * @param args
     */
    public void read(String...args) throws BadInputException {
        String[] tmp = args;
        for (int i = 1; i < args.length; i += 2)
        {
            try
            {
                Integer.parseInt(tmp[i]);
            }
            catch (NumberFormatException e)
            {
                throw new BadInputException();
            }
        }

    }
}

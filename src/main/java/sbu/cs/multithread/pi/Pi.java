package sbu.cs.multithread.pi;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Pi
{
    private BigDecimal pi;
    private int digits;
    private MathContext mc;

    public Pi(int digits)
    {
        this.digits = digits;
        mc = new MathContext(digits);
        pi = new BigDecimal(0);
    }

    public BigDecimal compute()
    {
        CountDownLatch count = new CountDownLatch(digits);
        //ExecutorService service = Executors.newFixedThreadPool(10);

        for (int i = 0; i < digits; i++)
        {
            int finalI = i;
            Thread thread = new Thread(() ->
            {
                synchronized (" ")
                {
                    pi = pi.add(piFunction(finalI));
                    count.countDown();
                }
            });
            //service.submit(thread);
            thread.start();
        }

        //service.shutdown();

        try
        {
            count.await();
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }

        return pi;
    }

    private BigDecimal piFunction(int k)
    {
        int k8 = 8 * k;

        BigDecimal val1 = new BigDecimal(4);
        val1 = val1.divide(new BigDecimal(k8 + 1), mc);
        BigDecimal val2 = new BigDecimal(-2);
        val2 = val2.divide(new BigDecimal(k8 + 4), mc);
        BigDecimal val3 = new BigDecimal(-1);
        val3 = val3.divide(new BigDecimal(k8 + 5), mc);
        BigDecimal val4 = new BigDecimal(-1);
        val4 = val4.divide(new BigDecimal(k8 + 6), mc);

        BigDecimal val = val1;

        val = val.add(val2);
        val = val.add(val3);
        val = val.add(val4);

        BigDecimal multiplier = new BigDecimal(16);
        multiplier = multiplier.pow(k);

        BigDecimal one = new BigDecimal(1);
        multiplier = one.divide(multiplier, mc);
        val = val.multiply(multiplier);

        return val;
    }
}
